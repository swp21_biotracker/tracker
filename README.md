# Biotracker 

## Binaries (Windows & Linux)

- [BioTracker core application](https://git.imp.fu-berlin.de/bioroboticslab/biotracker/biotracker/pipelines)
- [Background subtraction tracker](https://git.imp.fu-berlin.de/bioroboticslab/biotracker/backgroundsubtraction_tracker/pipelines)
- [Biotracker Interfaces](https://git.imp.fu-berlin.de/bioroboticslab/biotracker/interfaces/pipelines)
- [Biotracker Utility](https://git.imp.fu-berlin.de/bioroboticslab/biotracker/interfaces/pipelines)

## Usage Example

- start biotracker
- load tracker plugin (backgroundsubtraction_tracker)
- set number of tracked objects to 2
- start playback

![](background_subtraction.png)

Note: Tracking of robofish does not work.

## Building Biotracker Plugin

**Skip to step 3 if you are using the [robofish dockerfile](https://git.imp.fu-berlin.de/bioroboticslab/robofish/docker).**

1. Install Build Dependencies

- opencv (> 3.0)
- QT (>= 5.4)
- Boost
- CMake (>= 3.13)
- Buildtools (Tested: MSVC buildtools or g++)
- recommended: ninja
- vtk *(TODO: add to official docs)*

2. Build Biotracker Plugin Dependencies

```sh

# clone utility and interfaces
git clone https://git.imp.fu-berlin.de/bioroboticslab/biotracker/utility
git clone https://git.imp.fu-berlin.de/bioroboticslab/biotracker/interfaces

DEPENDENCY_INSTALL_PATH=~/git/biotracker/deps

# build interfaces (run in interfaces repository)
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=$DEPENDENCY_INSTALL_PATH ..
make install

# build utility (run in utility repository)
mkdir build && cd build
cmake \
    -DCMAKE_INSTALL_PREFIX=$DEPENDENCY_INSTALL_PATH \
    -DCMAKE_PREFIX_PATH=$DEPENDENCY_INSTALL_PATH \
make install
```

3. Build Biotracker plugin

Example: [backgroundsubtraction_tracker](https://git.imp.fu-berlin.de/bioroboticslab/biotracker/backgroundsubtraction_tracker)

```sh
# build backgroundsubtraction_tracker (run in backgroundsubtraction_tracker repository)
git clone https://git.imp.fu-berlin.de/bioroboticslab/biotracker/backgroundsubtraction_tracker
mkdir build && cd build
cmake -DCMAKE_PREFIX_PATH=$DEPENDENCY_INSTALL_PATH
make install
```
You should now have a library at `build/Src/biotracker-backgroundSubtraction.bio_tracker.so` that you can load in the
GUI!

## Building Biotracker

Biotracker Plugin & Core must be built against the same libraries. To build biotracker core, build `behavior_loader`
using the same commands as for `utility`. Then build biotracker core using all the previous libraries.
